---
sidebar: false
---

# The game

## Big picture

*The discovery of magic changes revolutionises the technological age*

## Dos and Don'ts

| Yes | No | 
----|---
| Mechanical animals produce biological products. | Time travel can change history  |
| Magic is harnessed through incantations (signed or spoken)  |  Telekinesis |
| Animals in space  | Average life expectancy above 90  |
| Androids and Cyborgs   | Supercombining mechs  |
|  | Self-aware AI |


## Foci

| Focus | Name |
---|---
| General Briggs | Nat |
| New habitable planet | Ben |
| Magic | David |

## Legacies

| Name | Legacy |
---|---
| Nathanael | Flying machines |
| Benedict | Utopion |
| David | Explosion of General Briggs |


## Cards

::: period-dark
(START)<br /><br />
As war ravages all nations, superpowers seek to harness a new power.
:::

::: event-light
New telescope leads to discovery of first habitable planet (X345W)
:::

::: event-light
Small faction made of all nations creates a foundation dedicated to colonising X345W
:::

::: event-light
General Briggs father dies in combat; galvanising him to become a great leader
:::

::: event-light
Young General Briggs falls in love and marries a poet
:::

::: event-dark
General Briggs authorises the culling of millions of innocent lives to further their side of conquest.
:::

::: event-dark
Following General Briggs assasination, civil war breaks out in the Cradden nation
:::

::: scene-dark
How was General Briggs assasinated? 

---

General Briggs' 50th wedding anniversary

---

He exploded after being poisoned. His wife is also killed in the explosion.

:::

::: event-light
A group forms about a century after General Briggs death, in his honour, that fulfills his dream of uniting the nations
:::

::: period-light
Following the end of centuries of war, international co-operation accelerates magic research
:::

::: event-dark
Underground organisation uses magic to commit crime
:::

::: event-dark
President of the world is assasinated by the Magica (the crime magic family)
:::

::: period-light
Humanity sends it's first waves of colonists into space
:::

::: event-light
Humanity launches it's first collossal ship into space, to find Utopion (X345W), never to return to Earth.
:::

::: scene-light

What happened when the first ship launched for Utopion?

---

The whole world watches as the first collosal ship leaves Earth.

---

The ship launches after a few technical hitches, leaving some countries against the action.

:::

::: event-light
On Earth, Flying machines become ubiquitous and reliant completely on magic.
:::

::: event-dark
Several months into their journey, one of the colonist ships suffers a serious fault, losing half the people on board
:::

::: period-light
Colonisation of the first found habitable planet outside of the solar system.
:::

::: event-dark
Colonisation efforts hit drawbacks as native alien species rebel.
:::

::: event-light
Special minerals found on Utopion that enhance capabilites of colonists' machinery.
:::

::: event-light
Cooperation with the native species allows for the use of magic for healing.
:::

::: period-light
Mankind learns how to move through space instantaneously
:::

::: event-light
Use of instantaneous travel becomes highly regulated, travel only between specific areas legal
:::

::: period-light
(END)<br />
Through sustained exposure to magic over millenia, mankind gains the ability to walk through time and space unhindered.
:::
