# Microscope Friday

A Vuepress project for writing up a microscope game.

# Getting started

This project is a standard [Vuepress](https://vuepress.vuejs.org/) project. 
To start developing you will need

* The latest Node.js 10.11 - https://nodejs.org/en/
    * (I prever using `nodeenv` on linux to install my node environments)
* A good javascript editor
    * Visual Studio Code is good - https://code.visualstudio.com/
* A connection the computer you're working on (so you can see the changes live in the browser)


## Essential reading

* The official docs - https://vuepress.vuejs.org/ - should be enough to get started
* When building Vue components - https://vuejs.org/v2/guide/components.html - we use standalone components (files ending in `.vue`) which are detailed in the docs
    * When using Vue.js (e.g. when making components), it's good to do the 'Getting Started' with Vue.js first https://vuejs.org/

## Starting the project
Once the repo is cloned and you are using node 10, do:

```
npm install
```

in the git directory to install all the dependencies. To run the development server, do

```
npm start
```
It will reload the page every time you make a change.
